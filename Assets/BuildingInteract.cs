﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingInteract : MonoBehaviour {

    public GameObject[] floorArr;
    public Transform cameraCenter;

    public GameObject buildingB;

    private void Start()
    {
        Physics.autoSimulation = false;
    }

    public void SwitchFloor(int floor)
    {
        for (int i =0; i < floorArr.Length; i++)
        {
            floorArr[i].gameObject.SetActive(false);
            if(i <= floor)
            {
                floorArr[i].SetActive(true);
            }
            if (i == floor)
            {
                cameraCenter.position = new Vector3(cameraCenter.position.x, floorArr[i].transform.position.y, cameraCenter.position.z);
            }
        }

    }

    public void ToggleB()
    {
        if (buildingB.active)
        {
            buildingB.SetActive(false);
        }
        else
        {
            buildingB.SetActive(true);
        }
    }
}
