﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyFadeAnimation : MonoBehaviour {

    public GameObject obj;
    private bool canFade;
    private Color alphaColor;
    private float timeToFade = 1.0f;

    public void Start()
    {
        canFade = true;
        alphaColor = obj.GetComponent<MeshRenderer>().material.color;
        alphaColor.a = 0;
    }
    public void Update()
    {
        if (canFade)
        {
            obj.GetComponent<MeshRenderer>().material.color =  new Color(1, 1, 1, Mathf.Lerp(1, 0, 1f));
        }
    }
}
